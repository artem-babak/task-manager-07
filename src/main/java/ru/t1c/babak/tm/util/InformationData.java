package ru.t1c.babak.tm.util;

public class InformationData {

    private long valueByte;

    private long prettyValue;

    private String prettySuffix;

    private String pretty;

    private String prettyValueByte;

    /**
     * Construct object; calc all pretty fields.
     */
    public InformationData(final long valueByte) {
        this.valueByte = valueByte;
        calcPretty();
        calcPrettyValueByte();
    }

    /**
     * Set valueByte; recalc all pretty fields.
     * @param valueByte
     */
    public void setValueByte(long valueByte) {
        this.valueByte = valueByte;
        calcPretty();
        calcPrettyValueByte();
    }

    public long getValueByte() {
        return valueByte;
    }

    public String getPrettyValueByte() {
        return prettyValueByte;
    }

    /**
     * Data value in pretty format
     * @return formatted string value
     */
    public String getPretty() {
        return pretty;
    }

    /**
     * Calc prettyValue, prettySuffix value from valueByte
     */
    private void calcPretty() {
        final long bytes = this.valueByte;
        final long kilobyte = 1024;
        final long megabyte = kilobyte * 1024;
        final long gigabyte = megabyte * 1024;
        final long terabyte = gigabyte * 1024;
        if ((bytes >= 0) && (bytes < kilobyte)) {
            prettyValue = bytes;
            prettySuffix = "B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            prettyValue = (bytes / kilobyte);
            prettySuffix = "KB";
        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            prettyValue = (bytes / megabyte);
            prettySuffix = "MB";
        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            prettyValue = (bytes / gigabyte);
            prettySuffix = "GB";
        } else if (bytes >= terabyte) {
            prettyValue = (bytes / terabyte);
            prettySuffix = "TB";
        } else {
            prettyValue = bytes;
            prettySuffix = "B";
            calcPrettyValueByte();
            pretty = prettyValueByte + " " + prettySuffix;
            return;
        }
        pretty = Long.toString(prettyValue) + " " + prettySuffix;
    }

    /**
     * Calc prettyValueByte value from valueByte
     */
    private void calcPrettyValueByte() {
        this.prettyValueByte = calcPrettySimple(this.valueByte);
    }

    /**
     * Convert long number to pretty string with every 3 digit spacing.
     * 123456789 -> 123 456 789
     *
     * @param value
     * @return formatted string value
     */
    private String calcPrettySimple(final long value) {
        if ((value > 0 && value < 1000) || (value < 0 && value > -1000))
            return Long.toString(value);
        StringBuilder prettyValue = new StringBuilder();
        for(long a = value; a > 0; a /= 1000)
            prettyValue.insert(0, a%1000 + " ");
        return prettyValue.toString();
    }

    @Override
    public String toString() {
        return getPretty();
    }

}
